INSERT INTO franchise (description, name)
VALUES ('An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.', 'Harry Potter');

INSERT INTO franchise (description, name)
VALUES ('The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil.', 'Lord of the Rings');

INSERT INTO movie (director, genre, poster, release_year, title, trailer, franchise_id)
VALUES ('Chris Columbus', 'Adventure, Family, Fantasy', 'https://m.media-amazon.com/images/M/MV5BMzkyZGFlOWQtZjFlMi00N2YwLWE2OWQtYTgxY2NkNmM1NjMwXkEyXkFqcGdeQXVyNjY1NTM1MzA@.V1.jpg',
        '2001', 'Harry Potter and the Philosopher´s Stone', 'https://youtu.be/VyHV0BRtdxo', '1');

INSERT INTO movie (director, genre, poster, release_year, title, trailer, franchise_id)
VALUES ('Chris Columbus', 'Adventure, Family, Fantasy', 'https://www.imdb.com/title/tt0295297/mediaviewer/rm3790637825/?ref_=tt_ov_i',
        '2002', 'Harry Potter and the Chamber of Secrets', 'https://www.youtube.com/watch?v=s4Fh2WQ2Xbk', '1');

INSERT INTO movie (director, genre, poster, release_year, title, trailer, franchise_id)
VALUES ('Peter Jackson', 'Action, Adventure, Drama, Fantasy', 'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg',
        '2001', 'The Fellowship of the Ring', 'https://youtu.be/V75dMMIW2B4', '2');

INSERT INTO movie (director, genre, poster, release_year, title, trailer, franchise_id)
VALUES ('Peter Jackson', 'Action, Adventure, Drama, Fantasy', 'https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@.V1.jpg',
        '2002', 'The Two Towers', 'https://youtu.be/LbfMDwc4azU', '2');

INSERT INTO movie_character (alias, full_name, gender, picture)
VALUES ('Mr Underhill', 'Frodo Baggins', 'Male', 'https://en.wikipedia.org/wiki/Frodo_Baggins#/media/File:Elijah_Wood_as_Frodo_Baggins.png');

INSERT INTO movie_character (alias, full_name, gender, picture)
VALUES ('Sam', 'Samwise Gamgee', 'Male', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRo2KZIl9h0rKR3cC3ktBSZHi2-K4dCLJQnQhGFQdDqMaZoW5z0pLv2CBCheHWWjpAx0o&usqp=CAU');

INSERT INTO movie_character (alias, full_name, gender, picture)
VALUES ('The Boy Who Lived', 'Harry Potter', 'Male', 'https://en.wikipedia.org/wiki/Harry_Potter_(character)#/media/File:Harry_Potter_character_poster.jpg');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('1', '3');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('1', '4');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('2', '3');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('2', '4');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('3', '1');

INSERT INTO moviecharacter_movie (moviecharacter_id, movie_id)
VALUES ('3', '2');