package com.scania.moviecharactersapi.models.dtos;

import java.util.List;

public class CharactersInFranchiseDto {
    private String franchise;
    private List<String> movieCharacters;

    public CharactersInFranchiseDto(String franchise, List<String> movieCharacters) {
        this.franchise = franchise;
        this.movieCharacters = movieCharacters;
    }

    public String getFranchise() {
        return franchise;
    }

    public void setFranchise(String franchise) {
        this.franchise = franchise;
    }

    public List<String> getMovieCharacters() {
        return movieCharacters;
    }

    public void setMovieCharacters(List<String> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }
}
