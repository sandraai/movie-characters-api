package com.scania.moviecharactersapi.models.dtos;

import java.util.List;

public class CharactersInMovieDto {
    private String movie;
    private List<String> movieCharacters;


    public CharactersInMovieDto(String movie, List<String> movieCharacters) {
        this.movie = movie;
        this.movieCharacters = movieCharacters;

    }

    public List<String> getMovieCharacters() {
        return movieCharacters;
    }

    public void setMovieCharacters(List<String> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movieTitle) {
        this.movie = movieTitle;
    }
}
