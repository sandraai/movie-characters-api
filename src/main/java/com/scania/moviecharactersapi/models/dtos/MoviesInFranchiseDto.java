package com.scania.moviecharactersapi.models.dtos;

import java.util.List;

public class MoviesInFranchiseDto {
    private String franchise;
    private List<String> movies;


    public MoviesInFranchiseDto(String franchise, List<String> movies) {
        this.movies = movies;
        this.franchise = franchise;
    }

    public List<String> getMovies() {
        return movies;
    }

    public void setMovies(List<String> movies) {
        this.movies = movies;
    }

    public String getFranchise() {
        return franchise;
    }

    public void setFranchise(String franchise) {
        this.franchise = franchise;
    }
}
