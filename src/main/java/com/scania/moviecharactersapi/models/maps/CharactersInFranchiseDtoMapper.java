package com.scania.moviecharactersapi.models.maps;

import com.scania.moviecharactersapi.models.dtos.CharactersInFranchiseDto;
import com.scania.moviecharactersapi.models.domain.Franchise;
import com.scania.moviecharactersapi.models.domain.Movie;
import com.scania.moviecharactersapi.models.domain.MovieCharacter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CharactersInFranchiseDtoMapper {

    public CharactersInFranchiseDto getAllMovieCharactersInFranchise (Franchise franchise) {
        Map<Long, String> movieCharacterMap = new HashMap<>();
        List<Movie> movieList = franchise.getMovieList();
        for (Movie movie : movieList) {
            List<MovieCharacter> movieCharacterList = movie.getMovieCharacterList();
            for (MovieCharacter movieCharacter : movieCharacterList) {
                if (!movieCharacterMap.containsKey(movieCharacter.getId())) {
                    movieCharacterMap.put(movieCharacter.getId(), movieCharacter.getFullName());
                }
            }
        }
        CharactersInFranchiseDto charactersInFranchiseDto = new CharactersInFranchiseDto(franchise.getName(), new ArrayList<>(movieCharacterMap.values()));
        return charactersInFranchiseDto;
    }
}
