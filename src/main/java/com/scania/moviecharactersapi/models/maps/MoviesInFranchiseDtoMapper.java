package com.scania.moviecharactersapi.models.maps;

import com.scania.moviecharactersapi.models.dtos.MoviesInFranchiseDto;
import com.scania.moviecharactersapi.models.domain.Franchise;
import com.scania.moviecharactersapi.models.domain.Movie;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MoviesInFranchiseDtoMapper {

    public MoviesInFranchiseDto getAllMoviesInFranchise (Franchise franchise) {
        List<Movie> movieList = franchise.getMovieList();
        List<String> dtoList = new ArrayList<>();
        for (Movie movie : movieList) {
            dtoList.add(movie.getTitle());
        }
        MoviesInFranchiseDto moviesInFranchiseDto = new MoviesInFranchiseDto(franchise.getName(), dtoList);
        return moviesInFranchiseDto;
    }
}
