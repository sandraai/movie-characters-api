package com.scania.moviecharactersapi.models.maps;

import com.scania.moviecharactersapi.models.dtos.CharactersInMovieDto;
import com.scania.moviecharactersapi.models.domain.Movie;
import com.scania.moviecharactersapi.models.domain.MovieCharacter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CharactersInMovieDtoMapper {

    public CharactersInMovieDto getAllMovieCharactersInMovie (Movie movie) {
        List<MovieCharacter> characterList = movie.getMovieCharacterList();
        List<String> dtoList = new ArrayList<>();
        for (MovieCharacter movieCharacter : characterList) {
            dtoList.add(movieCharacter.getFullName());
        }
        CharactersInMovieDto charactersInMovieDto = new CharactersInMovieDto(movie.getTitle(), dtoList);
        return charactersInMovieDto;
    }
}
