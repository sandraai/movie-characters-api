package com.scania.moviecharactersapi.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.scania.moviecharactersapi.models.domain.Movie;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", nullable = false, length = 150)
    private String fullName;
    @Column(name = "alias")
    private String alias;
    @Column(name = "gender", length = 20)
    private String gender;
    @Column(name = "picture")
    private String pictureUrl;

    @ManyToMany
    @JoinTable(
            name = "moviecharacter_movie",
            joinColumns = {@JoinColumn(name = "moviecharacter_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    public List<Movie> movieList;

    @JsonGetter("movieList")
    public List<String> movies() {
        if (movieList != null) {
            return movieList.stream()
                    .map(movie -> "/api/v1/movie/" + movie.getId()).collect(Collectors.toList());
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
