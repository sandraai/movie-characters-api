package com.scania.moviecharactersapi.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 150)
    private String title;
    @Column(name = "genre", length = 100)
    private String genre;
    @Column(name = "release_year")
    private int releaseYear;
    @Column(name = "director", length = 150)
    private String director;
    @Column(name = "poster")
    private String posterUrl;
    @Column(name = "trailer")
    private String trailerUrl;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchise() {
        if (franchise != null) {
            return "/api/v1/franchise/" + franchise.getId();
        }
        return null;
    }

    @ManyToMany
    @JoinTable(
            name = "moviecharacter_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "moviecharacter_id")}
    )
    public List<MovieCharacter> movieCharacterList;

    @JsonGetter("movieCharacterList")
    public List<String> movieCharacters() {
        if (movieCharacterList != null) {
            return movieCharacterList.stream()
                    .map(movieCharacter -> "/api/v1/moviecharacter/" + movieCharacter.getId()).collect(Collectors.toList());
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public List<MovieCharacter> getMovieCharacterList() {
        return movieCharacterList;
    }

    public void setMovieCharacterList(List<MovieCharacter> movieCharacterList) {
        this.movieCharacterList = movieCharacterList;
    }
}
