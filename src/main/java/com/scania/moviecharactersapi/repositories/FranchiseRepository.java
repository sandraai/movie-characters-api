package com.scania.moviecharactersapi.repositories;

import com.scania.moviecharactersapi.models.domain.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
