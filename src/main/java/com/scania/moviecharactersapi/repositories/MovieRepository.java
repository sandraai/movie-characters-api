package com.scania.moviecharactersapi.repositories;

import com.scania.moviecharactersapi.models.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    
}
