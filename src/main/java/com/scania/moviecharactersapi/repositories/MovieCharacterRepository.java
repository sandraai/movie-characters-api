package com.scania.moviecharactersapi.repositories;

import com.scania.moviecharactersapi.models.domain.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Long> {
}
