package com.scania.moviecharactersapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Movie Character API", version = "1.0", description = "An API for a movie database, storing information about movies, their characters and franchises"))
public class MovieCharactersApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieCharactersApiApplication.class, args);
    }

}
