package com.scania.moviecharactersapi.controllers;

import com.scania.moviecharactersapi.models.dtos.CharactersInMovieDto;
import com.scania.moviecharactersapi.models.maps.CharactersInMovieDtoMapper;
import com.scania.moviecharactersapi.models.domain.Movie;
import com.scania.moviecharactersapi.models.domain.MovieCharacter;
import com.scania.moviecharactersapi.repositories.MovieCharacterRepository;
import com.scania.moviecharactersapi.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/movie")
@Tag(name = "Movie", description = "This resource represents a movie")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieCharacterRepository movieCharacterRepository;
    @Autowired
    private CharactersInMovieDtoMapper charactersInMovieDtoMapper;

    @Operation(summary = "Retrieves all movies which currently exist in the database")
    @GetMapping
    public ResponseEntity<List<Movie>> getMovies() {
        return new ResponseEntity<>(movieRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific movie by movie id from the database")
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie (@PathVariable Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(movieRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new movie to the database")
    @PostMapping
    public ResponseEntity<Movie> addMovie (@RequestBody Movie movie) {
        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific movie by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie (@PathVariable Long id, @RequestBody Movie movie) {
        if (!id.equals(movie.getId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.OK);
    }

    @Operation(summary = "Removes a specific movie by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie (@PathVariable Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        movieRepository.deleteById(id);
        return new ResponseEntity<>("Movie with id " + id + " was deleted.", HttpStatus.OK);
    }

    @Operation(summary = "Updates the movie characters, specified by an array of id's, in a movie specified by id")
    @PatchMapping("/{id}/moviecharacters")
    public ResponseEntity<Movie> updateMovieCharacters (@PathVariable Long id, @RequestBody int[] movieCharacterIdArray) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        List<MovieCharacter> movieCharacterList = new ArrayList<>();
        for (int movieCharacterId : movieCharacterIdArray) {
            if (!movieCharacterRepository.existsById((long) movieCharacterId)) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            movieCharacterList.add(movieCharacterRepository.findById((long) movieCharacterId).get());
        }
        Movie movie = movieRepository.findById(id).get();
        movie.setMovieCharacterList(movieCharacterList);

        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves all the movie characters as a list of their names, in a movie specified by id")
    @GetMapping("/{id}/moviecharacters")
    public ResponseEntity<CharactersInMovieDto> getAllCharactersInMovieDto (@PathVariable Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Movie movie = movieRepository.findById(id).get();
        return new ResponseEntity<>(charactersInMovieDtoMapper.getAllMovieCharactersInMovie(movie), HttpStatus.OK);
    }
}
