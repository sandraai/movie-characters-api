package com.scania.moviecharactersapi.controllers;

import com.scania.moviecharactersapi.models.domain.MovieCharacter;
import com.scania.moviecharactersapi.repositories.MovieCharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/moviecharacter")
@Tag(name = "Movie Character", description = "This resource represents a movie character")
public class MovieCharacterController {
    @Autowired
    private MovieCharacterRepository movieCharacterRepository;

    @Operation(summary = "Retrieves all movie characters which currently exist in the database")
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters () {
        return new ResponseEntity<>(movieCharacterRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific movie character by id from the database")
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter (@PathVariable Long id) {
        if (!movieCharacterRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(movieCharacterRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new movie character to the database")
    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter (@RequestBody MovieCharacter movieCharacter) {
        return new ResponseEntity<>(movieCharacterRepository.save(movieCharacter), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific movie character by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter
            (@PathVariable Long id, @RequestBody MovieCharacter movieCharacter) {
        if (!id.equals(movieCharacter.getId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!movieCharacterRepository.existsById(id)) {
            return new ResponseEntity<>(movieCharacterRepository.save(movieCharacter), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(movieCharacterRepository.save(movieCharacter), HttpStatus.OK);
    }

    @Operation(summary = "Removes a specific movie character by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter (@PathVariable Long id) {
        if (!movieCharacterRepository.existsById(id)) {
            return new ResponseEntity<>("There is no such Character", HttpStatus.NOT_FOUND);
        }
        movieCharacterRepository.deleteById(id);
        return new ResponseEntity<>("Character " + id + " has been deleted.", HttpStatus.OK);
    }
}
