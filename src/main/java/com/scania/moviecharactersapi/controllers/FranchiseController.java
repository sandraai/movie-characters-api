package com.scania.moviecharactersapi.controllers;

import com.scania.moviecharactersapi.models.dtos.CharactersInFranchiseDto;
import com.scania.moviecharactersapi.models.dtos.MoviesInFranchiseDto;
import com.scania.moviecharactersapi.models.maps.CharactersInFranchiseDtoMapper;
import com.scania.moviecharactersapi.models.maps.MoviesInFranchiseDtoMapper;
import com.scania.moviecharactersapi.models.domain.Franchise;
import com.scania.moviecharactersapi.models.domain.Movie;
import com.scania.moviecharactersapi.repositories.FranchiseRepository;
import com.scania.moviecharactersapi.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/franchise")
@Tag(name = "Franchise", description = "This resource represents a franchise")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharactersInFranchiseDtoMapper charactersInFranchiseDtoMapper;
    @Autowired
    private MoviesInFranchiseDtoMapper moviesInFranchiseDtoMapper;

    @Operation(summary = "Retrieves all franchises which currently exist in the database")
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchise () {
        return new ResponseEntity<>(franchiseRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific franchise by franchise id from the database")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise (@PathVariable Long id) {
        if (!franchiseRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(franchiseRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new franchise to the database")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise (@RequestBody Franchise franchise) {
        return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific franchise by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise
            (@PathVariable Long id, @RequestBody Franchise franchise) {
        if (!id.equals(franchise.getId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.OK);
    }

    @Operation(summary = "Removes a specific franchise by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise (@PathVariable Long id) {
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>("There is no such Franchise", HttpStatus.NOT_FOUND);
        }
        franchiseRepository.deleteById(id);
        return new ResponseEntity<>("Franchise " + id + " has been deleted.", HttpStatus.OK);
    }

    @Operation(summary = "Updates the movies, specified by an array of id's, in a franchise specified by id")
    @PatchMapping("/{id}/movies")
    public ResponseEntity<Franchise> updateMovies (@PathVariable Long id, @RequestBody int[] movieIdArray) {
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        List<Movie> movieList = new ArrayList<>();
        for (int movieId : movieIdArray) {
            if (!movieRepository.existsById((long) movieId)) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            movieList.add(movieRepository.findById((long) movieId).get());
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        franchise.setMovieList(movieList);
        return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves all the movies as a list of their titles, in a franchise specified by id")
    @GetMapping("/{id}/movies")
    public ResponseEntity<MoviesInFranchiseDto> getAllMoviesInFranchise (@PathVariable Long id) {
        if (!franchiseRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        return new ResponseEntity<>(moviesInFranchiseDtoMapper.getAllMoviesInFranchise(franchise), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves all the movie characters as a list of their names, in a franchise specified by id")
    @GetMapping("/{id}/moviecharacters")
    public ResponseEntity<CharactersInFranchiseDto> getAllMovieCharactersInFranchise (@PathVariable Long id) {
        if (!franchiseRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        return new ResponseEntity<>(charactersInFranchiseDtoMapper.getAllMovieCharactersInFranchise(franchise), HttpStatus.OK);
    }
}
